module impractical.co/auth/sessions

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/pkg/errors v0.8.1
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	yall.in v0.0.1
)
